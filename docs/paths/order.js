module.exports = {
    post:{
        tags:['Order'],
        description: "Order a product",
        operationId: "postOrder",
        parameters:[],
        responses:{
            '201':{
                description:"Order created successfully",
                content:{
                    'application/json':{
                        schema:{
                            $ref:"#/components/schemas/order"
                        }
                    }
                }
            },
            '400': {
                description: 'Bad request, maybe check your authentication token?'
            },
            '500': {
                description: 'Server error'
            }
        }
    }
}