const products = require('./products')
const status = require('./status')
const order = require('./order')

module.exports = {
    paths:{
        '/api/products':{
            ...products
        },
        '/api/status':{
            ...status
        },
        '/api/order': {
            ...order
        }
    }
}