module.exports = {
    get:{
        tags:['Products'],
        description: "Get all products",
        operationId: "getProducts",
        parameters:[],
        responses:{
            '200':{
                description:"Products sent",
                content:{
                    'application/json':{
                        schema:{
                            $ref:"#/components/schemas/products"
                        }
                    }
                }
            },
            '404': {
                description: 'No products available'
            },
            '500': {
                description: "Server error", // response desc
            },
        }
    }
}