module.exports = {
    get:{
        tags:['Status'],
        description: "Get machine status",
        operationId: "getStatus",
        parameters:[],
        responses:{
            '200':{
                description:"Status sent",
                content:{
                    'application/json':{
                        schema:{
                            $ref:"#/components/schemas/status"
                        }
                    }
                }
            },
            '500': {
                description: 'Server error'
            }
        }
    }
}