module.exports = {
    openapi:"3.0.1",
    info:{
        version:"1.0.0",
        title:"Industrie 4.0 API",
        description: "API für Industrie 4.0",
    }
}