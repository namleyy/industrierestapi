module.exports = {
    components: {
        schemas: {
            order:{
                type:'object',
                properties:{
                    requestId:{
                        type:'integer',
                        description:"Order identification number",
                        example:1
                    },
                    name:{
                        type:'string',
                        description:"Name of the person ordering",
                        example:"Marvin"
                    },
                    product:{
                        type:"string",
                        description:"the name of the product you wanted to order",
                        example:'Orange Juice'
                    },
                    amount: {
                        type:'integer',
                        description: 'Amount of juice you want to order',
                        example: 10
                    }
                }
            },
            products: {
                type: "object",
                properties: {
                    aJuice: {
                        type: "object",
                        properties: {
                            litres: {
                                type: "string",
                                description: 'Amount of juice you want to order in litre'
                            },
                        }
                    },
                    bJuice: {
                        type: "object",
                        properties: {
                            litres: {
                                type: "string",
                                description: 'Amount of juice you want to order in litre'
                            },
                        }
                    },
                    cJuice: {
                        type: "object",
                        properties: {
                            litres: {
                                type: "string",
                                description: 'Amount of juice you want to order in litre'
                            },
                        }
                    }
                }
            },
            status: {
                type: 'object',
                properties: {
                    aJuice: {
                        type: 'object',
                        properties: {
                            id: {
                                type:'integer'
                            },
                            message: {
                                type:'object',
                                properties: {
                                    temp: {
                                        type:'integer',
                                        description:'temperature of the machine',
                                        example: -30
                                    },
                                    level: {
                                        type:'integer',
                                        description:'level',
                                        example: 34
                                    },
                                    pump1: {
                                        type:'integer',
                                        description:'Pumping machine number #1',
                                        example: 23
                                    },
                                    pump2: {
                                        type:'integer',
                                        description:'Pumping machine number #2',
                                        example: 23
                                    },
                                    pump3: {
                                        type:'integer',
                                        description:'Pumping machine number #3',
                                        example: 23
                                    },
                                    pump4: {
                                        type:'integer',
                                        description:'Pumping machine number #4',
                                        example: 23
                                    },
                                }
                            }
                        }
                    },
                    bJuice: {
                        type: 'object',
                        properties: {
                            id: {
                                type:'integer'
                            },
                            message: {
                                type:'object',
                                properties: {
                                    temp: {
                                        type:'integer',
                                        description:'temperature of the machine',
                                        example: -30
                                    },
                                    level: {
                                        type:'integer',
                                        description:'level',
                                        example: 34
                                    },
                                    pump1: {
                                        type:'integer',
                                        description:'Pumping machine number #1',
                                        example: 23
                                    },
                                    pump2: {
                                        type:'integer',
                                        description:'Pumping machine number #2',
                                        example: 23
                                    },
                                    pump3: {
                                        type:'integer',
                                        description:'Pumping machine number #3',
                                        example: 23
                                    },
                                    pump4: {
                                        type:'integer',
                                        description:'Pumping machine number #4',
                                        example: 23
                                    },
                                }
                            }
                        }
                    },
                    cJuice: {
                        type: 'object',
                        properties: {
                            id: {
                                type:'integer'
                            },
                            message: {
                                type:'object',
                                properties: {
                                    temp: {
                                        type:'integer',
                                        description:'temperature of the machine',
                                        example: -30
                                    },
                                    level: {
                                        type:'integer',
                                        description:'level',
                                        example: 34
                                    },
                                    pump1: {
                                        type:'integer',
                                        description:'Pumping machine number #1',
                                        example: 23
                                    },
                                    pump2: {
                                        type:'integer',
                                        description:'Pumping machine number #2',
                                        example: 23
                                    },
                                    pump3: {
                                        type:'integer',
                                        description:'Pumping machine number #3',
                                        example: 23
                                    },
                                    pump4: {
                                        type:'integer',
                                        description:'Pumping machine number #4',
                                        example: 23
                                    },
                                }
                            }
                        }
                    }
                },
            }
        }
    }
};
