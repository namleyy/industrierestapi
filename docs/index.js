const basicInfos = require('./basicInfos');
const servers = require('./servers');
const components = require('./components');
const paths = require('./paths/index')

module.exports = {
    ...basicInfos,
    ...servers,
    ...components,
    ...paths,
};