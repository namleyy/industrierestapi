const express = require("express");
const docs = require('./docs');
const mqtt = require('mqtt');
const swaggerUI = require("swagger-ui-express");
const cors = require('cors');

const app = express();
const port = process.env.PORT || 4000;
const tokens = ['bs14', 'token123'];

let errorCode, errorMessage, statusMessage;

let statusA = {
    id: 1,
    message: 'No messages'
};
let statusB = {
    id: 2,
    message: 'No messages'
};
let statusC = {
    id: 3,
    message: 'No messages'
};

app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(docs));
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.post('/api/order', function(req, res) {
    //res.status(400).send('no valid data was transmitted')
    let body, headers;

    body    = req.body;
    headers = req.headers;

    if (authenticate(headers) === false || validateData(body) === false) {
        res.status(errorCode).send(errorMessage);
        return false;
    }

    const call = connect('10.14.213.223:1883', 'R058/unprocessedOrders', body);

    let message = {
        message : 'Sent'
    }

    res.status(201).send(message)

});

app.get('/api/status', function(req, res) {

    listen('10.14.213.223:1883', 'R058/processDataJuice1', 1);
    listen('10.14.213.223:1883', 'R058/processDataJuice2', 2);
    listen('10.14.213.223:1883', 'R058/processDataJuice3', 3);

    var response = {
        aJuice : statusA,
        bJuice : statusB,
        cJuice : statusC
    }

    res.status(200).send(response)
});

app.get('/api/products', function (req, res) {

    const products = {
        "aJuice" : {
            'litres': '10'
        },
        "bJuice" : {
            'litres': '10'
        },
        "cJuice" : {
            'litres': '10'
        },
    };

    if(!products) {
        res.status(404).send('No Products available')
    }

    res.status(200).send(JSON.stringify(products));
});

function connect(address, topic, order) {

    const client  = mqtt.connect('mqtt://' + address)
    let answer;


    client.on('connect', function () {
        let json = JSON.stringify({
            Id      : order.requestId,
            Name    : order.name,
            Produkt : order.product,
            Anzahl  : order.amount
        });
        client.publish(topic, json)
    })

    client.on('message', function (topic, message) {
        // message is Buffer
        client.end()
        answer = message.toString();
    })

    return answer;
}

function validateData(params) {

    if (JSON.stringify(params) === '{}') {
        errorCode = 400;
        errorMessage = {
            id: 1,
            response:'No data was transmitted',
            errorCode: errorCode + " Bad Request"
        };
        return false;
    }

    if (
        params.requestId === undefined ||
        params.name      === undefined ||
        params.product   === undefined ||
        params.amount    === undefined
    ) {
        errorCode = 400;
        errorMessage = {
            id: 1,
            response:'No valid data was transmitted',
            errorCode: errorCode + " Bad Request"
        };
        return false;
    }

    return true;
}

function authenticate(headers) {
    if (tokens.indexOf(headers.authtoken) > -1) return true;

    errorCode = 400;
    errorMessage = {
        id:1,
        response: "Authentication failed",
        errorCode: errorCode + " Bad Request"
    };

    return false;
}

function listen (address, topic, id) {

    var listen  = mqtt.connect('mqtt://' + address)

    listen.subscribe(topic);

    listen.on('connect', function () {

        listen.on('message', function (topic, message) {
            var response = message.toString();
            statusMessage = {
                id: id,
                message: response
            }
            setStatus(id, statusMessage);
        });
    })
}

function setStatus(id, message) {
    if (id === 1) {
        statusA = message;
    } else if (id === 2) {
        statusB = message
    } else {
        statusC = message;
    }
}

async function initialize(){
    app.listen(port);
}

initialize()
    .finally(
        () => console.log(`app started on port:${port}`)
    );
